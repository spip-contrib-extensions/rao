<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_confirmer' => 'Confirmer',

	// E
	'erreur_auteurs_identiques' => 'Veuillez indiquer des auteurs différents !',

	// L
	'label_auteur_source' => 'Auteur source',
	'label_auteur_source_explication' => "Quel est l'auteur qui est désassocié ?",

	'label_auteur_destination' => 'Auteur destination',
	'label_auteur_destination_explication' => "Quel est l'auteur qui est réassocié ?",

	'label_objets' => 'Objets éditoriaux',
	'label_objets_explication' => "Quels objets éditoriaux seront affectés ?",

	// M
	'message_confirmation' => "Après confirmation, les objets listés seront réassociés à l'auteur de destination.",

	// N
	'aucune_liaison_trouvee' => "Aucune liaison trouvée",
	'une_liaison_trouvee' => "Une liaison trouvée :",
	'nb_liaisons_trouvees' => "@nb@ liaisons trouvées :",

	// R
	'rao_explication' => "Cette page vous permet d'affecter les articles (ou autres objets éditoriaux) d'un auteur (source) à un autre (destination). L'auteur source ne sera alors plus auteur des articles ; l'auteur destination le sera à sa place.",
	'rao_titre' => 'Réassocier auteurs objets',
	'reassociation_effectuee' => 'Réassociation effectuée',
);
