<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// R
	'rao_description' => 'Ce plugin offre un formulaire permettant de délier un auteur actuellement lié à des objets éditoriaux (tel que des articles), et d\'en lier un autre à la place. Il est ainsi possible d\'affecter tous les articles à l\'auteur «Camille» à la place de l\'auteur «Dominique». ',
	'rao_nom' => 'Réassocier auteurs objets',
	'rao_slogan' => 'Réaffecte un auteur à la place d\'un autre pour les objets éditoriaux sélectionnés.',
);